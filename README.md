**Para correr el proyecto desde la terminal:**

dotnet run

**Credenciales de usuario administrador (para Log in):**

- usuario: Admin
- contraseña: 123456

**Credenciales para conexion exitosa a base de datos:**

- Nombre base de datos: Proyecto
- servidor: localhost
- puerto: 3306
- usuario: root
- clave: password
