using MySql.Data.MySqlClient;
using System;
using System.Data;
using Entidades;

namespace DisenioLogin.Datos
{
    internal class Socios
    {
        public string Nuevo_Soc(Socio soc)
        {
            string salida;
            MySqlConnection sqlCon = new MySqlConnection();

            try
            {
                sqlCon = Conexion.getInstancia().CrearConexion();
                MySqlCommand comando = new MySqlCommand("NuevoSoc", sqlCon);
                comando.CommandType = CommandType.StoredProcedure;

                comando.Parameters.Add("Nom", MySqlDbType.VarChar).Value = soc.NombreS;
                comando.Parameters.Add("Ape", MySqlDbType.VarChar).Value = soc.ApellidoS;
                comando.Parameters.Add("Tel", MySqlDbType.Int32).Value = soc.TelefonoS;
                comando.Parameters.Add("Doc", MySqlDbType.Int32).Value = soc.DocS;

                MySqlParameter ParCodigo = new MySqlParameter();
                ParCodigo.ParameterName = "rta";
                ParCodigo.MySqlDbType = MySqlDbType.Int32;
                ParCodigo.Direction = ParameterDirection.Output;
                comando.Parameters.Add(ParCodigo);

                sqlCon.Open();
                comando.ExecuteNonQuery();
                salida = Convert.ToString(ParCodigo.Value);
            }
            catch (Exception ex)
            {
                salida = ex.Message;
            }
            finally
            {
                if (sqlCon.State == ConnectionState.Open)
                {
                    sqlCon.Close();
                }
            }
            return salida;
        }
    }
}