using Gtk;
using MySql.Data.MySqlClient;
using System;

namespace DisenioLogin.Datos
{
    public class Conexion
    {
        private string T_baseDatos;
        private string T_servidor;
        private string T_puerto;
        private string T_usuario;
        private string T_clave;
        private static Conexion? con = null;

        private Conexion()
        {
            ObtenerValoresPorTeclado();
        }
        private void ObtenerValoresPorTeclado()
        {
            this.T_baseDatos = valorDesdeInterfaz("Nombre de la base de datos");
            this.T_servidor = valorDesdeInterfaz("Servidor");
            this.T_puerto = valorDesdeInterfaz("Puerto");
            this.T_usuario = valorDesdeInterfaz("Nombre de usuario");
            this.T_clave = valorDesdeInterfaz("Clave");
        }
        private string valorDesdeInterfaz(string etiqueta)
        {
            var dialog = new MessageDialog(
                null,
                DialogFlags.Modal,
                MessageType.Question,
                ButtonsType.OkCancel,
                $"Ingrese {etiqueta}:"
            );

            var entry = new Entry();
            dialog.ContentArea.Add(new Label($"Ingrese {etiqueta}:"));
            dialog.ContentArea.Add(entry);

            dialog.ShowAll();

            string valor = "";

            if (dialog.Run() == (int)ResponseType.Ok)
            {
                valor = entry.Text;
            }

            dialog.Destroy();

            return valor;
        }

        public MySqlConnection CrearConexion()
        {
            MySqlConnection? cadena = new MySqlConnection();

            bool correcto = false;

            while (!correcto)
            {
                try
                {
                    cadena.ConnectionString = ObtenerCadenaConexion();
                    correcto = true;
                }
                catch (Exception)
                {
                    ObtenerValoresPorTeclado();
                }
            }

            return cadena;
        }

        public static Conexion getInstancia()
        {
            if (con == null)
            {
                con = new Conexion();
            }
            return con;
        }

        public string ObtenerCadenaConexion()
        {
            return $"datasource={this.T_servidor};" +
                   $"port={this.T_puerto};" +
                   $"username={this.T_usuario};" +
                   $"password={this.T_clave};" +
                   $"Database={this.T_baseDatos}";
        }
    }
}