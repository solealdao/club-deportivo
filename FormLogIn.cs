using Gtk;
using System;
using System.Data;
using DisenioLogin.Datos;
using DisenioLogin;

public partial class LoginForm : Window
{
    private frmPrincipal ventanaPrincipal;
    public LoginForm(frmPrincipal principal) : base(WindowType.Toplevel)
    {
        this.ventanaPrincipal = principal;
        SetDefaultSize(300, 200);
        SetPosition(WindowPosition.Center);

        var vbox = new Gtk.Box(Gtk.Orientation.Vertical, 0);

        var lblUsuario = new Label("Usuario:");
        var txtUsuario = new Entry();
        txtUsuario.Text = "USUARIO";
        txtUsuario.GrabFocus();
        txtUsuario.FocusOutEvent += (o, args) =>
        {
            if (string.IsNullOrWhiteSpace(txtUsuario.Text))
                txtUsuario.Text = "USUARIO";
        };

        var lblContrasena = new Label("Contraseña:");
        var txtContrasena = new Entry();
        txtContrasena.Text = "CONTRASEÑA";
        txtContrasena.Visibility = false;
        txtContrasena.FocusOutEvent += (o, args) =>
        {
            if (string.IsNullOrWhiteSpace(txtContrasena.Text))
            {
                txtContrasena.Text = "CONTRASEÑA";
                txtContrasena.Visibility = false;
            }
        };
        txtContrasena.FocusInEvent += (o, args) =>
        {
            if (txtContrasena.Text == "CONTRASEÑA")
            {
                txtContrasena.Text = string.Empty;
                txtContrasena.Visibility = true;
            }
        };

        var btnIniciarSesion = new Button("Iniciar Sesión");
        btnIniciarSesion.Clicked += (o, args) =>
        {
            string usuario = txtUsuario.Text;
            string contrasena = txtContrasena.Text;

            Usuarios usuarios = new Usuarios();

            try
            {
                DataTable tablaLogin = usuarios.Log_Usu(usuario, contrasena);

                if (tablaLogin.Rows.Count > 0)
                {
                    ShowMessage("Ingreso exitoso");
                    this.Hide();
                    frmPrincipal ventanaPrincipal = new frmPrincipal();
                    ventanaPrincipal.SetUsuario(usuario);
                    ventanaPrincipal.MostrarVentana();

                    return;
                }
                else
                {
                    ShowMessage("Usuario y/o contraseña incorrectos");
                }
            }
            catch (Exception ex)
            {
                ShowMessage($"Error: {ex.Message}");
            }
        };
        void ShowMessage(string message)
        {
            var dialog = new MessageDialog(this, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok, message);
            dialog.Run();
            dialog.Destroy();
        }

        vbox.PackStart(lblUsuario, false, false, 10);
        vbox.PackStart(txtUsuario, false, false, 10);
        vbox.PackStart(lblContrasena, false, false, 10);
        vbox.PackStart(txtContrasena, false, false, 10);
        vbox.PackStart(btnIniciarSesion, false, false, 10);

        Add(vbox);

        DeleteEvent += (o, args) => Application.Quit();
        ShowAll();
    }
}