using Gtk;
using System;
using DisenioLogin;
using Entidades;
using DisenioLogin.Datos;

public partial class FormInscripcion : Window
{
    private Entry txtNombre;
    private Entry txtApellido;
    private Entry txtDocumento;
    private Entry txtTelefono;
    private frmPrincipal principal;

    public FormInscripcion(frmPrincipal principal) : base("Inscripción")
    {
        SetDefaultSize(400, 200);
        SetPosition(WindowPosition.Center);

        this.principal = principal;

        txtNombre = new Entry();
        txtApellido = new Entry();
        txtDocumento = new Entry();
        txtTelefono = new Entry();

        var lblNombre = new Label("Nombre: (*)");
        var lblApellido = new Label("Apellido: (*)");
        var lblDocumento = new Label("Documento: (*)");
        var lblTelefono = new Label("Teléfono: (*)");

        var btnIngresar = new Button("Ingresar");
        btnIngresar.Clicked += BtnIngresar_Click;
        var btnLimpiar = new Button("Limpiar");
        btnLimpiar.Clicked += BtnLimpiar_Click;
        var btnVolver = new Button("Volver");
        btnVolver.Clicked += BtnVolver_Click;

        var vbox = new Gtk.Box(Gtk.Orientation.Vertical, 0);
        vbox.PackStart(lblNombre, false, false, 5);
        vbox.PackStart(txtNombre, false, false, 5);
        vbox.PackStart(lblApellido, false, false, 5);
        vbox.PackStart(txtApellido, false, false, 5);
        vbox.PackStart(lblDocumento, false, false, 5);
        vbox.PackStart(txtDocumento, false, false, 5);
        vbox.PackStart(lblTelefono, false, false, 5);
        vbox.PackStart(txtTelefono, false, false, 5);
        vbox.PackStart(btnIngresar, false, false, 10);
        vbox.PackStart(btnLimpiar, false, false, 10);
        vbox.PackStart(btnVolver, false, false, 10);

        Add(vbox);

        DeleteEvent += (o, args) => Application.Quit();
        ShowAll();
    }

    private void BtnIngresar_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(txtNombre.Text) ||
            string.IsNullOrWhiteSpace(txtApellido.Text) ||
            string.IsNullOrWhiteSpace(txtDocumento.Text) ||
            string.IsNullOrWhiteSpace(txtTelefono.Text))
        {
            ShowMessage("Debe completar datos requeridos (*)");
        }
        else
        {
            try
            {
                Socio nuevoSocio = new Socio
                {
                    NombreS = txtNombre.Text,
                    ApellidoS = txtApellido.Text,
                    TelefonoS = Convert.ToInt32(txtTelefono.Text),
                    DocS = Convert.ToInt32(txtDocumento.Text)
                };

                Socios socios = new Socios();

                string respuesta = socios.Nuevo_Soc(nuevoSocio);

                bool esNumero = int.TryParse(respuesta, out int codigo);

                if (esNumero)
                {
                    if (codigo == 1)
                    {
                        ShowMessage("SOCIO YA EXISTE");
                    }
                    else
                    {
                        ShowMessage($"Se almacenó con éxito con el código Nro {respuesta}");
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage($"Error: {ex.Message}");
            }
        }
    }

    private void BtnLimpiar_Click(object sender, EventArgs e)
    {
        txtNombre.Text = "";
        txtApellido.Text = "";
        txtDocumento.Text = "";
        txtTelefono.Text = "";
        txtNombre.GrabFocus();
    }

    private void BtnVolver_Click(object sender, EventArgs e)
    {
        this.Hide();
        principal.ShowAll();
    }

    private void ShowMessage(string message)
    {
        var dialog = new MessageDialog(this, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok, message);
        dialog.Run();
        dialog.Destroy();
    }
}