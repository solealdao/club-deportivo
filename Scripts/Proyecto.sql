drop database if exists Proyecto;
create database Proyecto;
use Proyecto;

create table roles(
RolUsu int,
NomRol varchar(30),
constraint primary key(RolUsu)
);

insert into roles values
(120,'Administrador'),
(121,'Empleado');

create table usuario(
CodUsu int auto_increment,
NombreUsu varchar (20),
PassUsu varchar (15),
RolUsu int,
Activo boolean default true,
constraint pk_usuario primary key (CodUsu),
constraint fk_usuario foreign key(RolUsu) references roles(RolUsu)
);

insert into usuario(CodUsu,NombreUsu,PassUsu,RolUsu) values
(26,'Admin','123456',120);

create table socio(
NSocio int auto_increment,
NombreS varchar(30),
ApellidoS varchar(40),
TelefonoS int,
DocS int,
constraint pk_socio primary key(NSocio)
);

create table pago(
IdPago int auto_increment,
SocioId int,
Monto float,
Fecha date,
constraint pk_pago primary key(IdPago),
constraint fk_pago foreign key (SocioId) references socio(NSocio)
);
