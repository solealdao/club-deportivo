 DELIMITER //

CREATE PROCEDURE NuevoSoc(IN Nom VARCHAR(30), IN Ape VARCHAR(40), IN Tel INT, IN Doc INT, OUT rta INT)
BEGIN
    DECLARE filas INT DEFAULT 0;
    DECLARE existe INT DEFAULT 0;
    
    SELECT COUNT(*) INTO filas FROM socio;

    IF filas = 0 THEN
        SET filas = 452; -- Consideramos este número como el primer número de socio
    ELSE
        -- Buscamos el último número de socio almacenado para sumarle una unidad y considerarla como PRIMARY KEY de la tabla
        SELECT MAX(NSocio) + 1 INTO filas FROM socio;

        -- Para saber si ya está almacenado el socio
        SELECT COUNT(*) INTO existe FROM socio WHERE DocS = Doc;
    END IF;

    IF existe = 0 THEN
        INSERT INTO socio VALUES (filas, Nom, Ape, Tel, Doc);
        SET rta = filas;
    ELSE
        SET rta = existe;
    END IF;
END //

DELIMITER ;
