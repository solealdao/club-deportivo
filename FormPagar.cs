using Gtk;
using System;
using MySql.Data.MySqlClient;

public partial class FormPagar : Window
{
    private Entry txtNumeroSocio;
    private Entry txtMonto;
    private RadioButton rbtnEfectivo;
    private RadioButton rbtnTarjeta;
    private Button btnPagar;
    private Button btnVolver;
    private Button btnComprobante;
    private frmPrincipal principal;

    private string usuarioPago;
    private float montoPago;
    private DateTime fechaPago;

    public FormPagar(frmPrincipal principal) : base("Pagar cuota")
    {
        SetDefaultSize(400, 200);
        DeleteEvent += Window_DeleteEvent;

        this.principal = principal;

        txtNumeroSocio = new Entry();
        txtMonto = new Entry();
        rbtnEfectivo = new RadioButton("Efectivo");
        rbtnTarjeta = new RadioButton(rbtnEfectivo, "Tarjeta");

        btnPagar = new Button("Pagar");
        btnPagar.Clicked += BtnPagar_Click;

        btnVolver = new Button("Volver");
        btnVolver.Clicked += BtnVolver_Click;

        btnComprobante = new Button("Comprobante");
        btnComprobante.Clicked += BtnComprobante_Click;
        btnComprobante.Sensitive = false;

        var vbox = new Gtk.Box(Gtk.Orientation.Vertical, 0);
        vbox.PackStart(new Label("Número de Socio:"), false, false, 10);
        vbox.PackStart(txtNumeroSocio, false, false, 10);
        vbox.PackStart(new Label("Monto:"), false, false, 10);
        vbox.PackStart(txtMonto, false, false, 10);
        vbox.PackStart(new Label("Método de Pago:"), false, false, 10);
        vbox.PackStart(rbtnEfectivo, false, false, 5);
        vbox.PackStart(rbtnTarjeta, false, false, 5);
        vbox.PackStart(btnPagar, false, false, 10);
        vbox.PackStart(btnVolver, false, false, 10);
        vbox.PackStart(btnComprobante, false, false, 10);
        Add(vbox);
    }

    private bool SocioRegistrado(int numeroSocio)
    {
        using (MySqlConnection connection = new MySqlConnection(ObtenerCadenaConexion()))
        {
            connection.Open();

            string sql = "SELECT COUNT(*) FROM socio WHERE NSocio = @numeroSocio";

            using (MySqlCommand cmd = new MySqlCommand(sql, connection))
            {
                cmd.Parameters.AddWithValue("@numeroSocio", numeroSocio);

                int count = Convert.ToInt32(cmd.ExecuteScalar());
                return count > 0;
            }
        }
    }

    private void BtnPagar_Click(object sender, EventArgs e)
    {
        try
        {
            if (!int.TryParse(txtNumeroSocio.Text, out int numeroSocio))
            {
                ShowMessage("Por favor, ingrese un número de socio válido.");
                return;
            }

            if (!float.TryParse(txtMonto.Text, out float monto))
            {
                ShowMessage("Por favor, ingrese un monto válido.");
                return;
            }

            if (!SocioRegistrado(numeroSocio))
            {
                ShowMessage("El número de socio no está registrado.");
                return;
            }

            if (ExistePagoParaSocioEnMesActual(numeroSocio))
            {
                ShowMessage("Ya se ha realizado un pago para este socio en el mes actual.");
                return;
            }

            string metodoPago = rbtnEfectivo.Active ? "Efectivo" : "Tarjeta";
            DateTime fechaVencimiento = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 10).AddMonths(1);

            if (fechaVencimiento.Month == 12)
            {
                fechaVencimiento = fechaVencimiento.AddYears(1).AddMonths(-12);
            }

            RealizarPago(numeroSocio, metodoPago, monto, fechaVencimiento);
            btnComprobante.Sensitive = true;

            ShowMessage("Pago realizado con éxito.");
        }
        catch (Exception ex)
        {
            ShowMessage($"Error al procesar el pago: {ex.Message}");
        }
    }

    private void RealizarPago(int numeroSocio, string metodoPago, float monto, DateTime fechaVencimiento)
    {
        using (MySqlConnection connection = new MySqlConnection(ObtenerCadenaConexion()))
        {
            connection.Open();
            string sql = "INSERT INTO pago (socioId, monto, fecha, fechaVencimiento) VALUES (@socioId, @monto, @fecha, @fechaVencimiento)";

            using (MySqlCommand cmd = new MySqlCommand(sql, connection))
            {
                cmd.Parameters.AddWithValue("@socioId", numeroSocio);
                cmd.Parameters.AddWithValue("@monto", monto);
                cmd.Parameters.AddWithValue("@fecha", DateTime.Now);
                cmd.Parameters.AddWithValue("@fechaVencimiento", fechaVencimiento);

                cmd.ExecuteNonQuery();
            }
        }
        usuarioPago = ObtenerUsuarioPorSocioId(numeroSocio);
        montoPago = monto;
        fechaPago = DateTime.Now;
    }

    private bool ExistePagoParaSocioEnMesActual(int numeroSocio)
    {
        using (MySqlConnection connection = new MySqlConnection(ObtenerCadenaConexion()))
        {
            connection.Open();

            string sql = "SELECT COUNT(*) FROM pago WHERE socioId = @socioId AND MONTH(fecha) = MONTH(CURRENT_DATE()) AND YEAR(fecha) = YEAR(CURRENT_DATE())";

            using (MySqlCommand cmd = new MySqlCommand(sql, connection))
            {
                cmd.Parameters.AddWithValue("@socioId", numeroSocio);

                int count = Convert.ToInt32(cmd.ExecuteScalar());
                return count > 0;
            }
        }
    }

    private void BtnVolver_Click(object sender, EventArgs e)
    {
        this.Hide();
        principal.ShowAll();
    }
    private void BtnComprobante_Click(object sender, EventArgs e)
    {
        FormFactura formFactura = new FormFactura(usuarioPago, montoPago, fechaPago);
        formFactura.ShowAll();
        this.Hide();
    }

    private void Window_DeleteEvent(object sender, DeleteEventArgs a)
    {
        this.Hide();
    }

    private void ShowMessage(string message)
    {
        var dialog = new MessageDialog(this, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok, message);
        dialog.Run();
        dialog.Destroy();
    }
    private string ObtenerCadenaConexion()
    {
        return DisenioLogin.Datos.Conexion.getInstancia().ObtenerCadenaConexion();
    }
    private string ObtenerUsuarioPorSocioId(int numeroSocio)
    {
        using (MySqlConnection connection = new MySqlConnection(ObtenerCadenaConexion()))
        {
            connection.Open();

            string sql = "SELECT NombreS FROM socio WHERE NSocio = @numeroSocio";

            using (MySqlCommand cmd = new MySqlCommand(sql, connection))
            {
                cmd.Parameters.AddWithValue("@numeroSocio", numeroSocio);

                object result = cmd.ExecuteScalar();

                if (result != null)
                {
                    return result.ToString();
                }
                else
                {
                    throw new InvalidOperationException($"No se encontró un usuario para el número de socio {numeroSocio}");
                }
            }
        }
    }
}