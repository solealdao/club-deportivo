using System;
using System.Collections.Generic;

namespace Entidades
{
    public class Socio
    {
        public int NSocio { get; set; }
        public string? NombreS { get; set; }
        public string? ApellidoS { get; set; }
        public int TelefonoS { get; set; }
        public int DocS { get; set; }

    }
}