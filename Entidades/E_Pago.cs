namespace Entidades;
public class Pago
{
    public int IdPago { get; set; }
    public int SocioId { get; set; }
    public float Monto { get; set; }
    public DateTime Fecha { get; set; }
    public DateTime FechaVencimiento { get; set; }
}