﻿using System;
using Gtk;
using DisenioLogin;

namespace club_deportivo
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Application.Init();

            frmPrincipal ventanaPrincipal = new frmPrincipal();

            LoginForm loginForm = new LoginForm(ventanaPrincipal);

            loginForm.ShowAll();

            Application.Run();
        }
    }
}