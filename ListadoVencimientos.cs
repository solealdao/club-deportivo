using Gtk;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using Entidades;

public class ListadoVencimientos : Window
{
    private List<Pago> pagosProximosVencimientos;
    private frmPrincipal ventanaPrincipal;

    public ListadoVencimientos(List<Pago> pagos, frmPrincipal ventanaPrincipal) : base("Listado de Próximos Vencimientos")

    {
        SetDefaultSize(600, 400);
        DeleteEvent += Window_DeleteEvent;

        this.ventanaPrincipal = ventanaPrincipal;

        pagosProximosVencimientos = pagos;

        var treeView = new TreeView();
        var columnNombre = new TreeViewColumn { Title = "Nombre" };
        var columnFechaVencimiento = new TreeViewColumn { Title = "Fecha de Vencimiento" };

        var cellNombre = new CellRendererText();
        var cellFechaVencimiento = new CellRendererText();

        columnNombre.PackStart(cellNombre, true);
        columnFechaVencimiento.PackStart(cellFechaVencimiento, true);

        columnNombre.AddAttribute(cellNombre, "text", 0);
        columnFechaVencimiento.AddAttribute(cellFechaVencimiento, "text", 1);

        treeView.AppendColumn(columnNombre);
        treeView.AppendColumn(columnFechaVencimiento);

        var listStore = new ListStore(typeof(string), typeof(string));

        try
        {
            using (MySqlConnection connection = new MySqlConnection(ObtenerCadenaConexion()))
            {
                connection.Open();

                string sql = "SELECT s.NombreS, MAX(p.FechaVencimiento) AS UltimaFechaVencimiento " +
                    "FROM pago p " +
                    "JOIN socio s ON p.SocioId = s.NSocio " +
                    "GROUP BY s.NombreS " +
                    "HAVING DATEDIFF(MAX(p.FechaVencimiento), CURDATE()) <= 5";

                using (MySqlCommand cmd = new MySqlCommand(sql, connection))
                {
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string nombreSocio = reader.GetString(0);
                            string fechaVencimiento = reader.GetDateTime(1).ToShortDateString();

                            listStore.AppendValues(nombreSocio, fechaVencimiento);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error en la consulta SQL: {ex.Message}");
        }

        treeView.Model = listStore;

        var vbox = new Gtk.Box(Gtk.Orientation.Vertical, 0);
        vbox.PackStart(treeView, true, true, 10);

        var btnVolver = new Button("Volver");
        btnVolver.Clicked += BtnVolver_Clicked;
        vbox.PackStart(btnVolver, false, false, 10);

        Add(vbox);
    }

    private void Window_DeleteEvent(object sender, DeleteEventArgs a)
    {
        this.Hide();
    }

    private void BtnVolver_Clicked(object sender, EventArgs e)
    {
        this.Hide();
        ventanaPrincipal.ShowAll();
    }

    private string ObtenerCadenaConexion()
    {
        return DisenioLogin.Datos.Conexion.getInstancia().ObtenerCadenaConexion();
    }
}