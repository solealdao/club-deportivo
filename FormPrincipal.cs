using System;
using Gtk;
using Entidades;

public partial class frmPrincipal : Window
{
    internal string? rol;
    internal string? usuario;

    private Label lblIngreso;
    private Button btnSalir;
    private Button btnInscribir;
    private Button btnPagarCuota;
    private Button btnProximosVencimientos;
    private FormInscripcion? ventanaInscripcion;
    private List<Pago> sociosProximosVencimientos;

    public frmPrincipal() : base("Mi Aplicación")
    {
        SetDefaultSize(400, 200);
        DeleteEvent += Window_DeleteEvent;

        lblIngreso = new Label();
        btnSalir = new Button("Salir");
        btnInscribir = new Button("Inscribir socio");
        btnPagarCuota = new Button("Pagar cuota mensual");
        btnProximosVencimientos = new Button("Obtener lista próximos vencimientos");




        btnSalir.Clicked += BtnSalir_Clicked;
        btnInscribir.Clicked += BtnInscribir_Clicked;
        btnPagarCuota.Clicked += BtnPagarCuota_Clicked;
        btnProximosVencimientos.Clicked += BtnProximosVencimientos_Clicked;

        var vbox = new Gtk.Box(Gtk.Orientation.Vertical, 0);
        vbox.PackStart(lblIngreso, false, false, 10);
        vbox.PackStart(btnSalir, false, false, 10);
        vbox.PackStart(btnInscribir, false, false, 10);
        vbox.PackStart(btnPagarCuota, false, false, 10);
        vbox.PackStart(btnProximosVencimientos, false, false, 10);
        Add(vbox);
    }

    private void BtnInscribir_Clicked(object sender, EventArgs e)
    {
        ventanaInscripcion = new FormInscripcion(this);
        ventanaInscripcion.ShowAll();
        this.Hide();
    }

    private void BtnSalir_Clicked(object sender, EventArgs e)
    {
        Application.Quit();
    }

    private void BtnPagarCuota_Clicked(object sender, EventArgs e)
    {
        FormPagar ventanaPagar = new FormPagar(this);
        ventanaPagar.ShowAll();
        this.Hide();
    }

    private void BtnProximosVencimientos_Clicked(object sender, EventArgs e)
    {
        ListadoVencimientos ventanaVencimientos = new ListadoVencimientos(sociosProximosVencimientos, this);
        ventanaVencimientos.ShowAll();
        this.Hide();
    }

    private void Window_DeleteEvent(object sender, DeleteEventArgs a)
    {
        Application.Quit();
    }

    public void SetUsuario(string usuario)
    {
        this.usuario = usuario;
        lblIngreso.Text = "USUARIO: " + usuario;
    }

    public void MostrarVentana()
    {
        lblIngreso.Text = "USUARIO: " + usuario;
        ShowAll();
    }
}