using Gtk;
using System;

public partial class FormFactura : Window
{
    private string? usuario;
    private float? monto;
    private DateTime? fecha;

    private Label? lblTitulo;
    private Label? lblUsuario;
    private Label? lblMonto;
    private Label? lblFecha;
    private Button? btnImprimir;
    private Button? btnCerrar;

    public FormFactura(string usuario, float monto, DateTime fecha) : base("Comprobante de Pago")
    {
        try
        {
            SetDefaultSize(400, 200);

            this.usuario = usuario;
            this.monto = monto;
            this.fecha = fecha;

            lblTitulo = new Label("COMPROBANTE DE PAGO");
            lblUsuario = new Label("Usuario: " + usuario);
            lblMonto = new Label("Monto pagado: " + monto);
            lblFecha = new Label("Fecha de pago: " + fecha);

            btnImprimir = new Button("Imprimir");
            btnImprimir.Clicked += BtnImprimir_Click;

            btnCerrar = new Button("Cerrar");
            btnCerrar.Clicked += BtnCerrar_Click;

            var vbox = new Gtk.Box(Gtk.Orientation.Vertical, 0);
            vbox.PackStart(lblTitulo, false, false, 10);
            vbox.PackStart(lblUsuario, false, false, 5);
            vbox.PackStart(lblMonto, false, false, 5);
            vbox.PackStart(lblFecha, false, false, 5);
            vbox.PackStart(btnImprimir, false, false, 10);
            vbox.PackStart(btnCerrar, false, false, 10);

            Add(vbox);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error en el constructor de FormFactura: {ex.Message}");
        }
    }
    private void BtnImprimir_Click(object sender, EventArgs e)
    {
        try
        {
            btnImprimir.Visible = false;

            PrintOperation printOperation = new PrintOperation();

            printOperation.BeginPrint += (s, args) =>
            {
                PrintSettings printSettings = new PrintSettings();

                printSettings.Orientation = PageOrientation.Portrait;

                printOperation.PrintSettings = printSettings;
            };

            printOperation.DrawPage += (s, args) =>
            {
                var cr = args.Context.CairoContext;
                int width = Allocation.Width;
                int height = Allocation.Height;

                using (var surface = new Cairo.ImageSurface(Cairo.Format.ARGB32, width, height))
                {
                    using (var cairoContext = new Cairo.Context(surface))
                    {
                        this.Draw(cairoContext);

                        cr.SetSourceSurface(surface, 0, 0);
                        cr.Paint();
                    }
                }
            };

            printOperation.EndPrint += (s, args) =>
            {
                Console.WriteLine("Fin de la impresión.");
            };

            printOperation.Run(PrintOperationAction.PrintDialog, this);

            btnImprimir.Visible = true;

            frmPrincipal principal = new frmPrincipal();
            principal.ShowAll();
            this.Hide();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error en BtnImprimir_Click: {ex.Message}");
        }
    }

    private void BtnCerrar_Click(object sender, EventArgs e)
    {
        this.Hide();
    }
}